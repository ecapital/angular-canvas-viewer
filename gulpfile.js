// including plugins
var gulp = require('gulp')
, minifyCss = require("gulp-minify-css")
, uglify = require("gulp-uglify")
, concat = require("gulp-concat")

// Concat files
gulp.task('files', function () {
  gulp.src(['./files/tiff.js', './files/pdf.compat.js', './files/FormatReader.js', './files/pdf.js','./files/CanvasViewer.js'])
    .pipe(concat('CanvasViewer.all.js'))
    .pipe(gulp.dest('./files'));
});

// Build and minify for production
gulp.task('dist', ['files'], function () {
    gulp.src(['./files/CanvasViewer.all.js'])
    .pipe(uglify())
    .pipe(concat('CanvasViewer.all.min.js'))
    .pipe(gulp.dest('./dist'));

    gulp.src(['./files/pdf.worker.js'])
    .pipe(uglify())
    .pipe(concat('pdf.worker.js'))
    .pipe(gulp.dest('./dist'));
    
    gulp.src(['./files/CanvasViewer.css'])
    .pipe(minifyCss())
    .pipe(concat('CanvasViewer.min.css'))
    .pipe(gulp.dest('./dist'));
});
